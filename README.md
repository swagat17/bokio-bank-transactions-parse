# Introduction
Bokio bank transaction parser. 

# Build 
```shell
mvn clean package install
```

# Running API Server
#### - Prerequisite
- Java 8
- Maven 3
- Git

# To Run The Project
#### - Using IDE
- Open compatible IDE find the file named as BokioWebApplication.
- Right click on that file and run as Java application.
- Go to the browser and hit http://localhost:8080.

#### - Using CMD
- Go to the project folder location open command prompt.
- Execute ```mvn clean package install``` command.
- Open target folder and run ```java -jar bokio-0.1.jar``` command 
- Go to the browser and hit http://localhost:8080.