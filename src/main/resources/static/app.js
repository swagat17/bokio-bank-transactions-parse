(function() {
	'use strict';
	angular.module('sbAdminApp', [ 'oc.lazyLoad', 'ui.router', 'ui.bootstrap', 'ngResource']);

	angular.module('sbAdminApp').config(configure);
	configure.$inject = [ '$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider' ];
	function configure ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider) {
		$ocLazyLoadProvider.config({
			debug:false,
			events:true,
		});

		$urlRouterProvider.otherwise('/app/home');

		$stateProvider.state('app', {
			url:'/app',
			templateUrl: 'modules/dashboard/main.html',
			resolve: {
				loadMyDirectives:function($ocLazyLoad){
					return $ocLazyLoad.load({
						name:'sbAdminApp',
						files:[ 'directives/header/header.js',
							'directives/header/header-notification/header-notification.js',
							'services/restAPIService.js' ]
					}),
					$ocLazyLoad.load({
						name:'ngAnimate',
						files:[ 'lib/angular-animate/angular-animate.js' ]
					})
				}
			}
		})
		.state('app.home',{
			url:'/home',
			controller: 'MainCtrl',
			controllerAs: 'mainCtrl',
			templateUrl:'modules/dashboard/home.html',
			resolve: {
				loadMyFiles:function($ocLazyLoad) {
					return $ocLazyLoad.load({
						name:'sbAdminApp',
						files:[ 'modules/dashboard/mainCtrl.js' ]
					})
				}
			}
		})
	}
})();


