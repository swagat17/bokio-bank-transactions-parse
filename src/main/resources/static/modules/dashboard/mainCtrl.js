(function() {
	'use strict';
	angular.module('sbAdminApp').controller('MainCtrl', mainCtrl);
	mainCtrl.inject = ['$rootScope','restAPIService','$timeout'];
	function mainCtrl($rootScope, restAPIService,$timeout) {
		// TODO: will get current status of application
		// customer wise and product wise tabs for quick look and action

		var self = this;
		self.generateReport = generateReport;
		self.pasteEvent = pasteEvent;
		self.clearCache = clearCache;
		self.transactionsReportRequest = [];
		self.parseTransactionsList = [];
		self.request = {};
		self.pastedTransactions;
		self.newJson;
		self.generateTransactionsReport = generateTransactionsReport;
		init();
		function init(){
//			localStorage.removeItem("tempStorage");
//			console.log(localStorage.getItem("tempStorage"));

			if(localStorage.getItem("tempStorage")){
				assignValue(localStorage.getItem("tempStorage"));
			}
			$timeout(function(){
				$('#transactions-dataTables').DataTable().destroy();
				$('#transactions-dataTables').DataTable()
			}, 1000);
		}
		function generateReport(){
			if(self.transactionsReportRequest.length === 0){
				alert("Please paste the transactions in the textbox");
			}else{
				$('html, body').animate({
					scrollTop: $("#transactionsFormAnchor").offset().top
				}, 500);
				
				$timeout(function(){
					generateTransactionsReport();
				}, 500);
			}
		}
		
		function pasteEvent(event){
			self.transactionsReportRequest.push(window.event.clipboardData.getData('text'));
//			alert(self.transactionsReportRequest);
		}
		
		function clearCache(){
			localStorage.clear();
		}
		function generateTransactionsReport(){
			self.pastedTransactions = "";
			self.request.transactions = self.transactionsReportRequest;
			var generateReportRequest = restAPIService.generateReport().save(self.request);
			$('#transactions-dataTables').DataTable().destroy();
			generateReportRequest.$promise.then(function(response) {
				console.log(response.message);	
				assignValue(response.message);
				localStorage.setItem("tempStorage", response.message);
				self.transactionsReportRequest = [];
				$timeout(function(){
					$('#transactions-dataTables').DataTable();
				}, 1000);
			});
		}
		function assignValue(message){
			var json = message;
			self.newJson = json.replace(/([a-zA-Z0-9]+?):/g, '"$1":');
			self.newJson = self.newJson.replace(/'/g, '"');
			self.parseTransactionsList = JSON.parse(self.newJson);
		}
	}
})();
