(function() {
	'use strict';
	angular.module('sbAdminApp').factory('restAPIService', restAPIService);

	restAPIService.$inject = [ '$http', '$resource', '$rootScope' ];

	function restAPIService($http, $resource, $rootScope) {
		return {
			generateReport : generateReport
		}
		function generateReport(){
			var url = "generate/report/";
			return $resource(url);
		}
	}
})();