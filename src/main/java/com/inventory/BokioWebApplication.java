package com.inventory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BokioWebApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(BokioWebApplication.class, args);
	}

}
