package com.inventory.controller;

import java.math.BigDecimal;
import java.text.BreakIterator;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Pattern;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.inventory.controller.dto.TransactionsReportRequestDTO;
import com.inventory.controller.dto.TransactionsReportResponseDTO;
import com.inventory.utils.JsonUtils;

@Controller
public class TransactionsReportController {

	@RequestMapping(value = "/generate/report", method = RequestMethod.POST)
	public @ResponseBody String generateTransactionsReport(@Valid @RequestBody TransactionsReportRequestDTO transactions) {
		
		ArrayList<TransactionsReportResponseDTO> TransactionsReportResponseDTOs = new ArrayList<TransactionsReportResponseDTO>();
		TransactionsReportResponseDTOs = parseTransactions(transactions);
		Gson gson = new Gson();
		String responce =  gson.toJson(TransactionsReportResponseDTOs);
		return JsonUtils.getMessage(responce);

	}

	private ArrayList<TransactionsReportResponseDTO> parseTransactions(TransactionsReportRequestDTO transactions) {
		BreakIterator breakIterator = BreakIterator.getWordInstance();
		TransactionsReportResponseDTO transactionsReportResponseDTO;
		ArrayList<TransactionsReportResponseDTO> TransactionsReports = new ArrayList<TransactionsReportResponseDTO>();
		LinkedHashMap<Integer, LinkedHashMap> overlapDetectionMap = new LinkedHashMap<Integer, LinkedHashMap>();
		Date date = new Date();
		Integer i;
		for( i = 0; i < transactions.getTransactions().size(); i++) {
			//Set setA = new HashSet();
			LinkedHashMap<String, TransactionsReportResponseDTO> setA = new LinkedHashMap<String, TransactionsReportResponseDTO>();
			String transaction = transactions.getTransactions().get(i);
			String[] lines = transaction.split("\\n+");
			
			for(int j = 0; j < lines.length; j++) {
				boolean isDate = false;
				boolean isNumber = false;
				String paymentDate = null;
				String description = null;
				String belopp = null;
				String saldo = null;
				String[] words = lines[j].split("\\t+");

				words = Arrays.stream(words).filter(value -> value != null && value.trim().length() > 0).toArray(size -> new String[size]);

				for(int k = 0; k < words.length-1; k++) {
					
					boolean localIsDate = true;
					boolean localIsNumber = true;
					try {
						Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(words[k].trim());
						Date date2 = new SimpleDateFormat("yyyy-MM-dd").parse(words[k+1].trim());
						if (date1.compareTo(date2) >= 0) {
				            paymentDate = words[k+1].trim();
				        }else {
				        	paymentDate = words[k].trim();
				        }
					} catch (ParseException e) {
						//e.printStackTrace();
						localIsDate = false;
					}
					try {
						if((Pattern.compile("[^0-9,]").matcher(words[k]).find())) {
							int number1 = Integer.parseInt(words[k].replaceAll("[,;\\s;\\t;.]", "").trim());
							int number2 = Integer.parseInt(words[k+1].replaceAll("[,;\\s;\\t;.]", "").trim());
							belopp = words[k].trim();
							saldo = words[k+1].trim();
							description = words[k-1].trim();
						}else {
							localIsNumber = false;
						}
						
					}catch (Exception e) {
						//e.printStackTrace();
						localIsNumber = false;
					}
					if(localIsDate) {
						k++;
						isDate = localIsDate;
					}
					if(localIsNumber) {
						k++;
						isNumber = localIsNumber;
					}
					
				}
				 
				if(isDate && isNumber) {
					transactionsReportResponseDTO = new TransactionsReportResponseDTO();
					transactionsReportResponseDTO.setBelopp(belopp);
					transactionsReportResponseDTO.setDescription(description);
					transactionsReportResponseDTO.setPaymentDate(paymentDate);
					transactionsReportResponseDTO.setSaldo(saldo);
					setA.put(lines[j].replaceAll("\\s+","").trim(), transactionsReportResponseDTO);
					//TransactionsReports.add(transactionsReportResponseDTO);
				}
			}
			if(i == 0) {
				overlapDetectionMap.put(i, setA);
			}else {
				
				for (Entry<Integer, LinkedHashMap> entry : overlapDetectionMap.entrySet()) {
					
					LinkedHashMap<String, TransactionsReportResponseDTO> existingMap = entry.getValue();
					Set existingValueSet = existingMap.keySet();
					Set newValueSet = setA.keySet();
					ArrayList<String> existingValueList = new ArrayList<String>();
					ArrayList<String> newValueList = new ArrayList<String>();
					existingValueList.addAll(existingValueSet);
					newValueList.addAll(newValueSet);
					int count = -1;
					boolean overlap = false;
						for(int m = 0; m < newValueList.size(); m++){
							count = m;
							for(int p = 0; p < existingValueList.size(); p++) {
								if(newValueList.get(m).equalsIgnoreCase(existingValueList.get(p))) {
									if(m < newValueList.size()) {
										m++;
										if(m == newValueList.size()) {
											overlap = true;
											break;
										}
									}
								}else {
									break;
								}
							}
						}
						if(overlap) {
							for(int l = count; l < newValueList.size(); l++) {
								setA.remove(newValueList.get(l));
							}
						}
				}
				overlapDetectionMap.put(i, setA);
			}
			
		}
		for (Entry<Integer, LinkedHashMap> entry : overlapDetectionMap.entrySet()) {
			LinkedHashMap<String, TransactionsReportResponseDTO> resultMap = entry.getValue();
			for (Entry<String, TransactionsReportResponseDTO> resultEntry : resultMap.entrySet()) {
				TransactionsReports.add(resultEntry.getValue());
			}
		}
		return TransactionsReports;
	}
}
