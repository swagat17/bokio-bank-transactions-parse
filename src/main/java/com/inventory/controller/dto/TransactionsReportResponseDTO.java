package com.inventory.controller.dto;

import java.math.BigDecimal;
import java.util.Date;

public class TransactionsReportResponseDTO {

	private String paymentDate;
	private String description;
	private String  belopp;
	private String  saldo;
	public String getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getBelopp() {
		return belopp;
	}
	public void setBelopp(String belopp) {
		this.belopp = belopp;
	}
	public String getSaldo() {
		return saldo;
	}
	public void setSaldo(String saldo) {
		this.saldo = saldo;
	}

	
}
