package com.inventory.controller.dto;

import java.util.List;

import org.hibernate.validator.constraints.NotEmpty;

public class TransactionsReportRequestDTO {

	@NotEmpty(message = "Transaction cannot be empty!")
	private List<String> transactions;

	public List<String> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<String> transactions) {
		this.transactions = transactions;
	}


}
